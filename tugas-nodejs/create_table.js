var db = require("./db_config_tugas");

db.connect(function (err) {
    if (err) throw err;

    // membuat database dan eksekusi

    let sql = `CREATE TABLE IF NOT EXISTS customer_bank
    (
        cust_id int(11) NOT NULL AUTO_INCREMENT,
        nama varchar(50) NOT NULL,
        alamat int(200) NOT NULL,
        kode_pos char(5) DEFAULT NULL,
        no_hp varchar(15) DEFAULT NULL,
        email varchar(50) DEFAULT NULL,
        PRIMARY KEY (cust_id)
    )`;

/*     let sql = `CREATE TABLE IF NOT EXISTS product
    (
        product_id int(11) NOT NULL AUTO_INCREMENT,
        product_name varchar(200) DEFAULT NULL,
        product_price int(11) DEFAULT NULL,
        PRIMARY KEY (product_id)
    )`; */

    // eksekusi
    db.query(sql, function (err, result) {
        if (err) throw err;
        console.log("Table created");
    });
});