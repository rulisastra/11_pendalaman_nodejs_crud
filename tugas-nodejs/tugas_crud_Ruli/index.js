// menggunakan path
const path = require('path');
const express = require('express');
const hbs = require('hbs');
const bodyParser = require('body-parser');
const mysql = require('mysql');
const app = express();

// membuat koneksi
const conn = mysql.createConnection({
  host: 'localhost',
  user: 'root',
  password: '',
  database: 'db_nodejs'
});

// koneksi ke database
conn.connect((err) =>{
  if(err) throw err;
  console.log('Mysql Connected...');
});

// setting ke views hbs file
app.set('views',path.join(__dirname,'views'));
// set view engine
app.set('view engine', 'hbs');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
// set folder public as static folder for static file
app.use('/assets',express.static(__dirname + '/public'));

// route ke homepage customer_bank
app.get('/',(req, res) => {
  let sql = "SELECT * FROM customer_bank";
  let query = conn.query(sql, (err, results) => {
    if(err) throw err;
    res.render('customer_view',{ // product_view
      results: results
    });
  });
});

// route untuk insert data ke db
app.post('/save',(req, res) => {
  let data = {nama: req.body.nama, alamat: req.body.alamat, kode_pos: req.body.kode_pos, no_hp: req.body.no_hp, email: req.body.email};
  let sql = "INSERT INTO customer_bank SET ?";
  let query = conn.query(sql, data,(err, results) => {
    if(err) throw err;
    res.redirect('/');
  });
});

//route untuk update data
app.post('/update',(req, res) => {
  let sql = "UPDATE customer_bank SET nama='"+req.body.nama+"', alamat='"+req.body.alamat+"', kode_pos='"+req.body.kode_pos+"', no_hp='"+req.body.no_hp+"', email='"+req.body.email+"' WHERE cust_id="+req.body.id;
  let query = conn.query(sql, (err, results) => {
    if(err) throw err;
    res.redirect('/');
  });
});

//route untuk delete data
app.post('/delete',(req, res) => {
  let sql = "DELETE FROM customer_bank WHERE cust_id="+req.body.cust_id+"";
  let query = conn.query(sql, (err, results) => {
    if(err) throw err;
      res.redirect('/');
  });
});

// server listening di 80001
app.listen(8001, () => {
  console.log('Server is running at port 8001');
});
