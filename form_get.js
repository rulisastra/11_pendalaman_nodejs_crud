var http = require('http');
var url = require('url');
var fs = require('fs'); // file system

http.createServer(function(request, response) {
    var q = url.parse(request.url,true);

    if(q.pathname == "/search/" && request.method === "GET") {
        // ambil parameter dari url. harus sama dengan func <name> di html
        // harus sama. perhatikan!
        var keyword = q.query.keyword;

        if(keyword) {
            // ambil daya dari form dengan metode GET (UTAMA!!!)
            response.writeHead(200, {'Content-Type': 'text/html'});
            response.write("<h3>Search Results:</h3>");
            response.write("<p>Anda mencari: <b>" + keyword + "</b></p>");
            response.write("<pre>Tidak ada hasil! maaf, website masih dalam pengembangan</pre>")
            response.end("<a href='/search/'>Kembali</a>");
        } else{
            // tampilkan form search jika error
            fs.readFile('search.html',(err,data) => {
                if (err) { // kirim balasan error
                response.writeHead(404, {'Content-Type': 'text/html'});
                return response.end("404 Not Found"); // eh, ada error wkwkk
                }
                // kirim form search.html
                response.writeHead(200, {'Content-Type': 'text/html'});
                response.write(data); // memanggil data
                return response.end();
            });
        }
    }
}).listen(8000);

console.log('server is running on http://localhost:8000');