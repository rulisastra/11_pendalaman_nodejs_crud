var db = require("./db_config");

db.connect(function (err) {
    if (err) throw err;

    // membuat database dan eksekusi
    let sql = `CREATE TABLE mahasiswa
    (
        npm CHAR(8) NOT NULL, 
        nama VARCHAR(255) NOT NULL,
        kelas CHAR(5) NOT NULL,
        alamat VARCHAR(255),
        PRIMARY KEY (npm)
    )`;

    // jangan lupa tanda petik satu disebelah keypad 1
    
    // eksekusi
    db.query(sql, function (err, result) {
        if (err) throw err;
        console.log("Table created");
    });
});

// kalau ngga bisa

// DROP TABLE IF EXISTS `tablename` ;
// FLUSH TABLES `tablename` ; /* or exclude `tablename` to flush all tables */
// CREATE TABLE `tablename` ...

// lalu restart mysql dan vscode :)