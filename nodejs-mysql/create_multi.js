var db = require("./db_config");

db.connect(function (err) {
    if (err) throw err;

    // membuat database dan eksekusi
    let sql = "INSERT INTO mahasiswa (npm, nama, kelas, alamat) VALUES ?";
    var values = [
         ['1234567', 'Asep', '4AA01', 'Bandung'],
         ['1234561', 'Febri', '4AA01', 'Bandung'],
         ['1234562', 'Putri', '4AA01', 'Palembang'],
         ['1234563', 'Amelia', '4AA01', 'Bandung'],
         ['1234564', 'Gilang', '4AA01', 'Jakarta']
    ];
   
    // jangan lupa tambahin [values]
    db.query(sql, [values], function (err, result) {
        if (err) throw err;
        console.log("multi record iserted!");
    });
});