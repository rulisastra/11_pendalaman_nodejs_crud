var db = require("./db_config");

db.connect(function (err) {
    if (err) throw err;

    // membuat database dan eksekusi
    let sql = "CREATE DATABASE mahasiswa";
    db.query(sql, function (err, result) {
        if (err) throw err;
        console.log("Database created!");
    });
});